import os
import sys

cfiles = [];
cppfiles = [];

f = open("compilenow.cmd",'w+');

def compile_c_files():
    global pwd,cfiles,f,p_name;
    directory = os.listdir(os.getcwd()+"\INC");
    cfiles = [];

    #print(directory);
    for files in directory:
        if(files[len(files)-1]=='c'):
            file = files[0:len(files)-2];
            #if(file != p_name):
            cfiles.append(file);

    #print(cfiles);
    #command = "gcc \"-I"+pwd+"\GTST\" \"-I"+pwd+"\INC\" -O0 -g3 -ftest-coverage -fprofile-arcs -Wall -c -fmessage-length=0 -o \"DBG\SRC\\"+p_name+".o\" \""+pwd+"\INC\\"+p_name+".c\""
    #print(command);
    #f.write(command+"\n");
        
    for file in cfiles:
        command = "gcc \"-I"+pwd+"\GTST\" \"-I"+pwd+"\INC\" -O0 -g3 -ftest-coverage -fprofile-arcs -Wall -c -fmessage-length=0 -o \"DBG\SRC\\"+file+".o\" \""+pwd+"\INC\\"+file+".c\""
        print(command);
        f.write(command+"\n");
        
def compile_test_code():
    global pwd,cfiles,cppfiles,f,p_name;
    directory = os.listdir(os.getcwd()+"\TST");
    cppfiles = [];

    #print(directory);
    for files in directory:
        #print(files[len(files)-3:len(files)-1]);
        if(files[len(files)-3:len(files)]=="cpp"):
            file = files[0:len(files)-4];
            cppfiles.append(file);

    #print(cppfiles);
    for file in cppfiles:
        command = "g++ \"-I"+pwd+"\GTST\" \"-I"+pwd+"\INC\" -O0 -g3 -ftest-coverage -fprofile-arcs -Wall -c -fmessage-length=0 -o \"DBG\TST\\"+file+".o\" \""+pwd+"\TST\\"+file+".cpp\""
        print(command);
        f.write(command+"\n");
        
    command = "g++ \"-I"+pwd+"\GTST\" \"-I"+pwd+"\INC\" -O0 -g3 -ftest-coverage -fprofile-arcs -Wall -c -fmessage-length=0 -o \"DBG\GTST\gtest\gtest-all.o\" \""+pwd+"\GTST\gtest\gtest-all.cc\""
    print(command);
    f.write(command+"\n");
    c_obj_file_paths = " ".join([("\"DBG\SRC\\"+cfile+".o\"") for cfile in cfiles]);
    cpp_obj_file_paths = " ".join([("\"DBG\TST\\"+cppfile+".o\"") for cppfile in cppfiles]);
    print(c_obj_file_paths);
    print(cpp_obj_file_paths);
    command = "g++ -ftest-coverage -fprofile-arcs -o "+p_name+" \"DBG\GTST\gtest\gtest-all.o\" "+c_obj_file_paths+" "+cpp_obj_file_paths+" -lpthread"
    print(command);
    f.write(command+"\n");
    
def run_test_code():
    global pwd,cfiles,cppfiles,p_name;
    command = p_name+".exe --gtest_output=xml:RPT\RESULTS\\"+p_name+"_results.xml > out.txt"
    print(command);
    f.write(command+"\n");

def get_coverage_reports():
    global pwd,cfiles,cppfiles,p_name;
    for cppfile in cppfiles:
        command = "gcov -o \""+pwd+"\DBG\TST\\"+cppfile+".gcno\" \""+pwd+"\TST\\"+cppfile+".cpp\""
        print(command);
        f.write(command+"\n");
    
    for cfile in cfiles:    
        command = "gcov -o \""+pwd+"\DBG\SRC\\"+cfile+".gcno\" \""+pwd+"\INC\\"+cfile+".c\""
        print(command);
        f.write(command+"\n");
        if(cfile[0] == "M"):
            actfl = cfile;

    command = "gcovr -r \""+pwd+"\" > cover.txt"
    print(command);
    f.write(command+"\n");
    command = "cd \""+pwd+"\RPT\COVERAGE\""
    print(command);
    f.write(command+"\n");
    command = "gcovr -r \""+pwd+"\" --html --html-details -o coverage-"+p_name+".html"
    print(command);
    f.write(command+"\n");
    command = "cd \""+pwd+"\""
    print(command);
    f.write(command+"\n");     
    command = "python testresults.py  \""+pwd+"\RPT\RESULTS\\"+p_name+"_results.xml\" \""+pwd+"\RPT\COVERAGE\coverage-"+p_name+".INC_"+actfl+".c.html\" \""+pwd+"\RPT\PDF\\"+p_name+".pdf\""
    print(command);
    f.write(command+"\n");

#pwd = os.getcwd();
pwd = "%mypath%"

f.write("set mypath=%cd%"+"\n");
f.write("for %%I in (.) do set ProjName=%%~nxI"+"\n");


#p_name = os.path.basename(os.getcwd());
p_name = "%ProjName%"


f.write("rmdir \"RPT\" /s /q"+"\n");
f.write("mkdir \"RPT\""+"\n");
f.write("mkdir \"RPT\COVERAGE\""+"\n");
f.write("mkdir \"RPT\PDF\""+"\n");
f.write("mkdir \"RPT\RESULTS\""+"\n");

print(p_name);
compile_c_files();
compile_test_code();
run_test_code();
get_coverage_reports();
f.write("pause"+"\n");
f.close();

#def write_cmd_file(file_path):
#    global PROJECT_NM;
#    f = open(file_path,"a");
    #cmd_file_contents=
"""
gcc "-I{pwd}\\{pnm}\\GTST" "-I{pwd}\\{pnm}\\INC" -O0 -g3 -ftest-coverage -fprofile-arcs -Wall -c -fmessage-length=0 -o "DBG\\SRC\\{pnm}.o" "{pwd}\\{pnm}\\INC\\{pnm}.cpp" 
echo "{pnm}.o created"
g++ "-I{pwd}\\{pnm}\\GTST" "-I{pwd}\\{pnm}\\INC" -O0 -g3 -ftest-coverage -fprofile-arcs -Wall -c -fmessage-length=0 -o "DBG\\SRC\\{pnm}.o" "{pwd}\\{pnm}\\SRC\\{pnm}.cpp" 
echo "{pnm}.o created"
g++ "-I{pwd}\\{pnm}\\GTST" "-I{pwd}\\{pnm}\\INC" -O0 -g3 -ftest-coverage -fprofile-arcs -Wall -c -fmessage-length=0 -o "DBG\\TST\\gtst_main.o" "{pwd}\\{pnm}\\TST\\gtst_main.cpp" 
echo "gtst_main.o created"
g++ "-I{pwd}\\{pnm}\\GTST" "-I{pwd}\\{pnm}\\INC" -O0 -g3 -ftest-coverage -fprofile-arcs -Wall -c -fmessage-length=0 -o "DBG\\GTST\\gtest\\gtest-all.o" "{pwd}\\{pnm}\\GTST\\gtest\\gtest-all.cc" 
echo "gtest-all.o created"
g++ "-I{pwd}\\{pnm}\\GTST" "-I{pwd}\\{pnm}\\INC" -O0 -g3 -ftest-coverage -fprofile-arcs -Wall -c -fmessage-length=0 -o "DBG\\TST\\gtst_{pnm}.o" "{pwd}\\{pnm}\\TST\\gtst_{pnm}.cpp" 
echo "gtst_{pnm}.o created"
g++ -ftest-coverage -fprofile-arcs -o {pnm} "DBG\\GTST\\gtest\\gtest-all.o" "DBG\\SRC\\{pnm}.o" "DBG\\TST\\gtst_main.o" "DBG\\TST\\gtst_{pnm}.o" -lpthread 
echo "{pnm}.exe created with coverage reports"
echo "running tests"
{pnm}.exe --gtest_output="xml:RPT\\RESULTS\\{pnm}_results.xml"
echo "running tests success"
echo "results saved at {pwd}\\{pnm}\\RPT\\{pnm}_results.xml"
gcov -o "{pwd}\\{pnm}\\DBG\\TST\\gtst_{pnm}.gcno" "\\TST\\gtst_{pnm}.cpp"
gcov -o "{pwd}\\{pnm}\\DBG\\TST\\gtst_main.gcno" "\\TST\\gtst_main.cpp"
gcov -o "{pwd}\\{pnm}\\DBG\\SRC\\{pnm}.gcno" "\\SRC\\{pnm}.cpp"
cd {pwd}\\{pnm}\\RPT\\COVERAGE
gcovr -r {pwd}\\{pnm}\\ --html --html-details -o coverage-{pnm}.html
pause
"""
    #f.write(cmd_file_contents.format(pwd = os.getcwd(),pnm = PROJECT_NM));
    #f.close()
