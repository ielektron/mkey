
/*

filename : gtst_main.cpp
author : sathiyanarayanan
created at : 2019-04-01 01:56:12.328823

*/
    

#include <stdio.h>
#include "gtest/gtest.h"

GTEST_API_ int main(int argc, char **argv) {
  printf("Running main() from gtest_main.cc\n");
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
        