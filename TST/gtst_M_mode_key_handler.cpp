
/*

filename : gtst_ui_test.cpp
author : sathiyanarayanan
created at : 2019-04-01 01:56:12.656993

*/
    
# include "gtest/gtest.h"
//# include "ui_test1.h"

//# include "M_mode_key_handler.c"
//# include "stub.c"

extern "C" {
# include "M_mode_key_handler.h"
#include "M_led_handler.h"
}

/*
TEST(testcase1, startup)
{
  EXPECT_EQ(EXT_KEY,m_u8MKEY_type_get());
  set_inputs(0,1);
  EXPECT_EQ(DEFAULT_MODE,m_vMKEY_monitor());//count1
  EXPECT_EQ(DEFAULT_MODE,m_vMKEY_monitor());//count2
  EXPECT_EQ(DISARM_MODE,m_vMKEY_monitor());//count3 mode changed count0
  EXPECT_EQ(DISARM_MODE,m_u8MKEY_sts_get());
  
  
  EXPECT_EQ(LED_ARM,get_led_name());//disarmed mode arm led will be switched off
  EXPECT_EQ(OFF,get_led_sts());
  
  m_u8MKEY_sts_set(DEFAULT_MODE);
  EXPECT_EQ(DEFAULT_MODE,m_u8MKEY_sts_get());
  set_inputs(1,0);
  EXPECT_EQ(DEFAULT_MODE,m_vMKEY_monitor());//count1
  EXPECT_EQ(DEFAULT_MODE,m_vMKEY_monitor());//count2
  EXPECT_EQ(PART_ARM_MODE,m_vMKEY_monitor());//count3 mode changed count0
  EXPECT_EQ(PART_ARM_MODE,m_u8MKEY_sts_get());
  
  EXPECT_EQ(LED_ARM,get_led_name());
  EXPECT_EQ(ON,get_led_sts());
  
  m_u8MKEY_sts_set(DEFAULT_MODE);//part armed mode arm led will be switched on
  EXPECT_EQ(DEFAULT_MODE,m_u8MKEY_sts_get());
  set_inputs(1,1);
  EXPECT_EQ(DEFAULT_MODE,m_vMKEY_monitor());//count1
  EXPECT_EQ(DEFAULT_MODE,m_vMKEY_monitor());//count2
  EXPECT_EQ(ARM_MODE,m_vMKEY_monitor());//count3 mode changed count0
  EXPECT_EQ(ARM_MODE,m_u8MKEY_sts_get());
  
  EXPECT_EQ(LED_ARM,get_led_name());//armed mode arm led will be switched on
  EXPECT_EQ(ON,get_led_sts());
  
}

TEST(testcase1, transitionfrom_disarm)
{
  m_u8MKEY_sts_set(DISARM_MODE);
  EXPECT_EQ(DISARM_MODE,m_u8MKEY_sts_get());
  
  EXPECT_EQ(EXT_KEY,m_u8MKEY_type_get());
  set_inputs(0,1);
  
  EXPECT_EQ(DISARM_MODE,m_vMKEY_monitor());//mode changed using m_u8MKEY_sts_set //mode doesnt change to new one,count0
  EXPECT_EQ(DEFAULT_MODE,m_vMKEY_monitor());//count0
  EXPECT_EQ(DEFAULT_MODE,m_vMKEY_monitor());//count0
  EXPECT_EQ(DEFAULT_MODE,m_vMKEY_monitor());//count0 
  EXPECT_EQ(DISARM_MODE,m_u8MKEY_sts_get());//count0
  
  m_u8MKEY_sts_set(DISARM_MODE);
  EXPECT_EQ(DISARM_MODE,m_u8MKEY_sts_get());//not a change
  set_inputs(1,0);
  EXPECT_EQ(DEFAULT_MODE,m_vMKEY_monitor());//count1
  EXPECT_EQ(DEFAULT_MODE,m_vMKEY_monitor());//count2
  EXPECT_EQ(PART_ARM_MODE,m_vMKEY_monitor());//count3 mode changed count0
  //EXPECT_EQ(PART_ARM_MODE,m_u8MKEY_sts_get());
  
  m_u8MKEY_sts_set(DISARM_MODE);
  EXPECT_EQ(DISARM_MODE,m_u8MKEY_sts_get());
  set_inputs(1,1);
  EXPECT_EQ(DISARM_MODE,m_vMKEY_monitor());//mode changed using m_u8MKEY_sts_set,count1
  EXPECT_EQ(DEFAULT_MODE,m_vMKEY_monitor());//count2
  EXPECT_EQ(ARM_MODE,m_vMKEY_monitor());//count3 mode changed count0
  //EXPECT_EQ(ARM_MODE,m_u8MKEY_sts_get());
}

TEST(testcase1, transitionfrom_partarm)
{
  EXPECT_EQ(EXT_KEY,m_u8MKEY_type_get());
  m_u8MKEY_sts_set(PART_ARM_MODE);
  EXPECT_EQ(PART_ARM_MODE,m_u8MKEY_sts_get());
  set_inputs(0,1);
  EXPECT_EQ(PART_ARM_MODE,m_vMKEY_monitor());//count1 
  EXPECT_EQ(DEFAULT_MODE,m_vMKEY_monitor());//count2
  EXPECT_EQ(DISARM_MODE,m_vMKEY_monitor());//count3 mode changed count0
  EXPECT_EQ(DISARM_MODE,m_u8MKEY_sts_get());
  
  m_u8MKEY_sts_set(PART_ARM_MODE);
  EXPECT_EQ(PART_ARM_MODE,m_u8MKEY_sts_get());
  set_inputs(1,0);
  EXPECT_EQ(PART_ARM_MODE,m_vMKEY_monitor());//mode changed using m_u8MKEY_sts_set //mode doesnt change to new one,count0
  EXPECT_EQ(DEFAULT_MODE,m_vMKEY_monitor());//count0
  EXPECT_EQ(DEFAULT_MODE,m_vMKEY_monitor());//count0
  EXPECT_EQ(PART_ARM_MODE,m_u8MKEY_sts_get());
  
  m_u8MKEY_sts_set(PART_ARM_MODE);
  EXPECT_EQ(PART_ARM_MODE,m_u8MKEY_sts_get());
  set_inputs(1,1);
  EXPECT_EQ(DEFAULT_MODE,m_vMKEY_monitor());//mode doesnt change using m_u8MKEY_sts_set //mode change to new one using keypins ,count1
  EXPECT_EQ(DEFAULT_MODE,m_vMKEY_monitor());//count2
  EXPECT_EQ(ARM_MODE,m_vMKEY_monitor());//count3 mode changed count0
  EXPECT_EQ(ARM_MODE,m_u8MKEY_sts_get());
}

TEST(testcase1, transitionfrom_arm)
{
  EXPECT_EQ(EXT_KEY,m_u8MKEY_type_get());
  m_u8MKEY_sts_set(ARM_MODE);
  EXPECT_EQ(ARM_MODE,m_u8MKEY_sts_get());
  set_inputs(0,1);
  EXPECT_EQ(DEFAULT_MODE,m_vMKEY_monitor());//mode doesnt change using m_u8MKEY_sts_set //mode change to new one using keypins ,count1
  EXPECT_EQ(DEFAULT_MODE,m_vMKEY_monitor());//count2
  EXPECT_EQ(DISARM_MODE,m_vMKEY_monitor());//count3 mode changed count0
  EXPECT_EQ(DISARM_MODE,m_u8MKEY_sts_get());
  
  m_u8MKEY_sts_set(ARM_MODE);
  EXPECT_EQ(ARM_MODE,m_u8MKEY_sts_get());
  set_inputs(1,0);
  EXPECT_EQ(ARM_MODE,m_vMKEY_monitor());//mode changed using m_u8MKEY_sts_set //mode change to new one using keypins ,count1
  EXPECT_EQ(DEFAULT_MODE,m_vMKEY_monitor());//count2
  EXPECT_EQ(PART_ARM_MODE,m_vMKEY_monitor());//count3
  EXPECT_EQ(PART_ARM_MODE,m_u8MKEY_sts_get());
  
  m_u8MKEY_sts_set(ARM_MODE);
  EXPECT_EQ(ARM_MODE,m_u8MKEY_sts_get());
  set_inputs(1,1);
  EXPECT_EQ(ARM_MODE,m_vMKEY_monitor());//mode changed using m_u8MKEY_sts_set //mode not change to new one using keypins ,count0
  EXPECT_EQ(DEFAULT_MODE,m_vMKEY_monitor());//count0
  EXPECT_EQ(DEFAULT_MODE,m_vMKEY_monitor());//count0
  EXPECT_EQ(DEFAULT_MODE,m_vMKEY_monitor());//count0
  EXPECT_EQ(ARM_MODE,m_u8MKEY_sts_get());
}
*/


TEST(testcase8, negativenegative)
{
  set_inputs(0,0);
  EXPECT_EQ(DEFAULT_MODE,m_vMKEY_monitor());
  EXPECT_EQ(DEFAULT_MODE,m_vMKEY_monitor());
  EXPECT_EQ(DEFAULT_MODE,m_vMKEY_monitor());
  EXPECT_EQ(DEFAULT_MODE,m_u8MKEY_sts_get());
}


