/*

filename : stub.c
author : sathiyanarayanan
created at : 2019-04-01 01:56:12.281970

*/

#ifndef INC_STUB_C_
#define INC_STUB_C_

#include "M_led_handler.h"

LED_NAME gchoice = LED_END;
G_CTL_CODES gcmd = NILL;

void m_u8LED_ctrl_led(LED_NAME choice,G_CTL_CODES cmd)
{
	gchoice = choice;
	gcmd = cmd;
}

LED_NAME get_led_name()
{
	LED_NAME ret = gchoice;
	return ret;
}

G_CTL_CODES get_led_sts()
{
	G_CTL_CODES ret = gcmd;
	return ret;
}



#endif /* INC_STUB_C_ */