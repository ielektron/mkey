set mypath=%cd%
for %%I in (.) do set ProjName=%%~nxI
rmdir "RPT" /s /q
mkdir "RPT"
mkdir "RPT\COVERAGE"
mkdir "RPT\PDF"
mkdir "RPT\RESULTS"
gcc "-I%mypath%\GTST" "-I%mypath%\INC" -O0 -g3 -ftest-coverage -fprofile-arcs -Wall -c -fmessage-length=0 -o "DBG\SRC\M_mode_key_handler.o" "%mypath%\INC\M_mode_key_handler.c"
gcc "-I%mypath%\GTST" "-I%mypath%\INC" -O0 -g3 -ftest-coverage -fprofile-arcs -Wall -c -fmessage-length=0 -o "DBG\SRC\stub.o" "%mypath%\INC\stub.c"
g++ "-I%mypath%\GTST" "-I%mypath%\INC" -O0 -g3 -ftest-coverage -fprofile-arcs -Wall -c -fmessage-length=0 -o "DBG\TST\gtst_main.o" "%mypath%\TST\gtst_main.cpp"
g++ "-I%mypath%\GTST" "-I%mypath%\INC" -O0 -g3 -ftest-coverage -fprofile-arcs -Wall -c -fmessage-length=0 -o "DBG\TST\gtst_M_mode_key_handler.o" "%mypath%\TST\gtst_M_mode_key_handler.cpp"
g++ "-I%mypath%\GTST" "-I%mypath%\INC" -O0 -g3 -ftest-coverage -fprofile-arcs -Wall -c -fmessage-length=0 -o "DBG\GTST\gtest\gtest-all.o" "%mypath%\GTST\gtest\gtest-all.cc"
g++ -ftest-coverage -fprofile-arcs -o %ProjName% "DBG\GTST\gtest\gtest-all.o" "DBG\SRC\M_mode_key_handler.o" "DBG\SRC\stub.o" "DBG\TST\gtst_main.o" "DBG\TST\gtst_M_mode_key_handler.o" -lpthread
%ProjName%.exe --gtest_output=xml:RPT\RESULTS\%ProjName%_results.xml > out.txt
gcov -o "%mypath%\DBG\TST\gtst_main.gcno" "%mypath%\TST\gtst_main.cpp"
gcov -o "%mypath%\DBG\TST\gtst_M_mode_key_handler.gcno" "%mypath%\TST\gtst_M_mode_key_handler.cpp"
gcov -o "%mypath%\DBG\SRC\M_mode_key_handler.gcno" "%mypath%\INC\M_mode_key_handler.c"
gcov -o "%mypath%\DBG\SRC\stub.gcno" "%mypath%\INC\stub.c"
gcovr -r "%mypath%" > cover.txt
cd "%mypath%\RPT\COVERAGE"
gcovr -r "%mypath%" --html --html-details -o coverage-%ProjName%.html
cd "%mypath%"
python testresults.py  "%mypath%\RPT\RESULTS\%ProjName%_results.xml" "%mypath%\RPT\COVERAGE\coverage-%ProjName%.INC_M_mode_key_handler.c.html" "%mypath%\RPT\PDF\%ProjName%.pdf"
pause
