import xml.etree.ElementTree as ET
import pdfkit 
import sys

path1 = str(sys.argv[1]);
path2 = str(sys.argv[2]);
path3 = str(sys.argv[3]);

tree = ET.parse(path1)
root = tree.getroot()

contents = """
<!DOCTYPE html>
<html>
<head>
<style>
table {{
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 50%;
}}

td {{
  border: 2px solid black;
  text-align: left;
  padding: 8px;
}}

th {{
  border: 2px solid black;
  text-align: left;
  padding: 8px;
}}

</style>
</head>
<body>

<h2>TEST SUMMARY</h2>

<table>
  <tr>
    <td style="color:blue">TIMESTAMP</td>
    <td style="color:blue">{TME}</td>
  </tr>
  <tr style="background-color:lightgreen;">
    <td>TOTAL TESTS CONDUCTED</td>
    <td>{TTS}</td>
  </tr>
  <tr style="background-color:pink;">
    <td>TESTS FAILED</td>
    <td>{TTF}</td>
  </tr>
</table>
<h2>CONSOLE OUTPUT</h2>
<p>{console_output}</p>
<h2>FILE WISE COVERAGE</h2>
<table>{coverage_report}</table>
</body>
</html>
"""
con_out="";
"""(output_file = open("out.txt",'r')

while line:
    single_line = output_file.readline()+"<br>"
    con_out = con_out + single_line;"""

with open("out.txt") as fp:
    lines = fp.readlines();
    #con_out = con_out + single_line;

for line in lines:
    line=line+"<br>";
    con_out = con_out + line;
    
print(con_out);

with open("cover.txt") as fp:
    lines = fp.readlines();

ROW = [];

cov_txt = open("cover.txt","w+");
row_content = """
<tr style="background-color:{clr};">
    <td>{FN}</td>
    <td>{COV}</td>
  </tr>
"""

header_content = """
<tr>
    <td style="color:blue">file name</td>
    <td style="color:blue">coverage</td>
  </tr>
"""



ROW.append(header_content);

for line in lines:
    if(line[0:3]=="INC")or(line[0:3]=="TST"):
        fnm = line[0:line.find(" ")+1]
        percent = line[line.rfind("%")-4:line.rfind("%")+1]
        sentence = fnm +"-------------"+percent;
        if(int(percent[0:-1])>=90):
            color = "lightgreen"
        elif((int(percent[0:-1])>=75)):
            color = "yellow"
        else:
            color = "pink"
        ROW.append(row_content.format(FN=fnm,COV=percent,clr=color));
        cov_txt.write(sentence+"\n");
    #elif(line[0:3]=="Fil"):
    #    sentence = line
    #    cov_txt.write(sentence+"\n");

tabular_coverage = " ".join(ROW);

        
cov_txt.close();

cov_out = " "
with open("cover.txt") as fp:
    lines = fp.readlines();
    #con_out = con_out + single_line;

for line in lines:
    line=line+"<br>";
    cov_out = cov_out + line;
    
print(cov_out);

        
f = open(path1[0:len(path1)-4]+".html","w+");
f.write(contents.format(TME = root.attrib['timestamp'],TTS = root.attrib['tests'],TTF = root.attrib['failures'],console_output = con_out,coverage_report=tabular_coverage));
f.close()

pdfkit.from_file([path1[0:len(path1)-4]+".html",path2], path3)

"""
for child in root:
    print(child.tag, child.attrib)
    for case in child:
        #if(child.tag =="failures")and((child.attrib =="1")):
            #print(child.tag, child.attrib)
        print(case.tag,case.attrib);

"""

"""import xlwt 
from xlwt import Workbook

# Workbook is created 
wb = Workbook() 

# add_sheet is used to create sheet. 
sheet1 = wb.add_sheet('test results')


sheet1.write(1, 1,"TEST SUITE");
sheet1.write(1, 2,"TEST CASE");
sheet1.write(1, 3,"TEST CONDITION");
sheet1.write(1, 4,"TEST STS");
sheet1.write(1, 5,"REMARKS");


wb.save('testresults.xls')
print("ok");
"""

